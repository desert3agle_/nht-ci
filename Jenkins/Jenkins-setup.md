# JENKINS MASTER SETUP
This guide is more focused towards a staging setup, although steps mentioned here are same for both production and staging setup of Jenkins Master.

## Instance type
- **t3.medium** ( staging, for production bigger instance should be used )

## Disks
1. Root disk ( 50 gb )
	- Contains the staging jenkins master data
  		- Jenkins data directory → **/data**
    	- Contains only plugins, config and tools
     	- Does not have prod workspaces and jobs as they are not required
      
2. Jenkins prod master data disk ( 200 GB )
	- Mounted at **/jenkins_prod_data**
	- Disk created from jenkins prod master data disk snapshot
	- Not required unless you want to copy some workspace or job for testing
	- Needs to be updated in some months by taking fresh snapshot of prod jenkins master and then remounting the disk created from the snapshot ( only if required )

## Jenkins Version
- **2.303.3** ( same as production )

- Docker Image tag → **2.303.3-jdk8** ( super important to use ‘-jdk8’ ) as newer jenkins versions now work on java 11 by default so we have to explicitly mention we want to use jdk8 for our jenkins.

## Initial Setup
1. Install docker on the newly spinned up box

2. Create **/data** and following subdirs:
	- **/data/jenkins/master/logs**
	- **/data/jenkins/master/jenkins_home**
	- **/data/jenkins/jenkins_conf**
  
3. Attach a volume created from production jenkins data disk snapshot and mount it to **/jenkins_prod_data**
	- Copy all the files present in **/jenkins_prod_data/jenkins/jenkins_conf/** to **/data/jenkins/jenkins_conf** except clouds.yaml
	- Change folder ownership from root to 10000 user ( this is the user id given to jenkins inside the master image )
	- **sudo chown -R 10000:10000 /data/**
	- Now you are ready to deploy Jenkins. Use the following docker command to do so:
```bash
docker run --restart=always -d -p 9090:8080 -p 60000:60000 \
						-v /data/jenkins/master/logs:/var/log/jenkins/ \
						-v /data/jenkins/master/jenkins_home:/var/jenkins_home \
						-v /data/jenkins/jenkins_conf:/jenkins_conf \
						--env JAVA_OPTS="-Xmx1024m" \
						--env="VIRTUAL_PORT=8080" \
						--env="VIRTUAL_HOST=jenkins-staging.xyz.com" \
						--name=jenkins-master \
						jenkins/jenkins:2.303.3-jdk8
``` 
**[ NOTE ] : Update JAVA_OPTS env as per your requirements.**

4. You can run nginx proxy for jenkins container using the following command:
```bash
docker run --restart=always -d --name=nginx \
					 --env="DOCKER_HOST=unix:///tmp/docker.sock" \
					 --volume="/var/run/docker.sock:/tmp/docker.sock:ro" \
					 -p 80:80 \
           jwilder/nginx-proxy
```
**Proxy to access master on port 80 with virtual_host endpoint.**

1. If everything is right, you should see **"Jenkins is starting up"** on the staging endpoint ( or,  instance-ip:9090 )

2. Tail the logs using the following command to get the initial login token:
	- tail -f **/data/jenkins/master/logs/jenkins.log**
	- This password can also be found at: **/var/jenkins_home/secrets/initialAdminPassword** inside the jenkins container.
	- Also, these are the Jenkins master logs, so whenever you face any issues, refer to these logs only.
  
3. Paste the initial password you got from logs to **"Unlock Jenkins"** page ( which will be the onboarding page ) and Continue.

4. Next, choose **"Select plugins to install"** on the **"Customize Jenkins"** page.

5. Select **"None"** on the next page and click on Install **( make sure no plugin is selected, we will install the plugins later )**.

6.  Next, create first admin user, this user will be used to login to jenkins for now and also to install plugins until our ldap is setup, after that this user will be automatically deleted by jenkins.

7.  Now select the jenkins url you like to use, also use this same endpoint in the **VIRTUAL_HOST** env variable in docker run command used to deploy jenkins in **step 3**.

8.  Jenkins initial setup is complete, click on **"Start using Jenkins"** to go to the dashboard.

## Plugin Install

1. Clone this repo on your local machine, and navigate to the Jenkins/scripts folder.

2. Update the **"creds"** file, which contains credentials in the format **username:password** to authenticate with jenkins, in our case the creds we used for the first admin user, i.e.
	-	**admin:admin**
  
3. Replace production jenkins url with your staging/new jenkins url in the **install_plugins.sh** file and run it. This will install all the plugins mentioned in the **Jenkins/scripts/plugins.txt** file.

4. Once all the plugins are installed, Jenkins will automatically restart.

5. Now since we had copied all the configs present in **jenkins_conf** folder to our staging jenkins data directory in earlier step, Jenkins will be configured automatically while restarting and you will have to login using your ldap creds.

6. **Congrats!** your staging Jenkins master is ready to be used now.

## Configuration

1. You can give your user admin access by making changes to the **/data/jenkins/jenkins_conf/authorization.yaml** file.

2. For every change you make to the yaml files present in **jenkins_conf** folder, you will have to reload the configs. This can be done in two ways:
	- With admin access 
  		- **Manage Jenkins --> Configuration as Code --> Reload existing configuration**
	- Without admin access or if your changes broke Jenkins :D
  		- simple docker restart of your jenkins staging container

**[Note]** Make sure to take a snapshot of root disk before making any breaking changes like installing a plugin with major dependencies or upgrading Jenkins to a newer version, else you might have to redo everything.

That's all! Now you can go as wild as you want on your staging Jenkins.
