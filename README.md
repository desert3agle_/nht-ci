## Jenkins

### Setup

### Brief explanation of Jenkins pipeline

- Added Global credentials for Docker 
- Configured to auto install docker in global tool configuration
- Set it to trigger on push through webhook

#### Pipeline

- Imports docker hub credentials in DOCKER_CREDS
- Imports dockerTool
- It has 4 stage clone > docker-build > docker-push > cleanup
- In "clone" stage it pulls the git repo
- In "docker-build" stage, it builds the Dockerfile in directory 'nht-ci/node-hello-world'
- In "docker-push" stage, it authenticats the with docker registry creds and pushes the image it built
- In 'cleanup' stage, it cleans up the residue

* Pipeline

![pipeline](screenshots/pipeline-all.png)

*  Built/Pushed
![build](screenshots/pipeline-build-success.png)

* Pipeline stages
![success](screenshots/pipeline.png)

* Webhook
![webhook](screenshots/webhook.png)

## Gitlab CI

### Runner setup

- Installed gitlab-runer in mac device
- Choose shell as the executor
- Used the tag local to get the jobs to run on it
- Connected to it via CLI and started it before the build

### Brief explanation of Gitlab CI pipeline

- It has 3 stage build > login > push
- Before start, it gets into "tcp-echo-server" directory
- In "build" stage, it package the app with maven and then builds the image
- In "login" stage, it logs in to docker hub registry with the password passed through env variable
- In "push" stage it pushes the docker image to dockerhub


* Pipeline Sucessful
![success](screenshots/gitlab-ci-success.png)